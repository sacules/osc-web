# OSC-Web

A small program to broadcast OSC packets using WebSockets. It consists of a server that intercepts and sends the packets to a client, which exposes them to a port in your computer.

# Building

You need Go installed, preferably a recent version. Then you can build the programs:

```console
go build -o server server.go
go build -o client client.go
```

# Usage
First you need to forward the ports in the network you want to run the server from. Then you can run it like this, for example:
```console
./server --port-osc 6448 --port-web 9999
```

In this case I've got a program, say SuperCollider, that's transmitting OSC to the port 6448, and I've forwarded the port 9999 in my router. Then, in my client, which can be the same computer or another one somewhere on the internet, I can run it like:

```console
./client --ip 127.0.0.1 --port-web 9999 --port-osc 77771
```

Notice that both `port-web` must be identical, and the `port-osc` can be any you want, a high number is usually recommended. Now I can listen to OSC from another program on my computer.
