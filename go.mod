module gitlab.com/sacules/sensorosc

go 1.15

require (
	github.com/gorilla/websocket v1.4.2
	github.com/hypebeast/go-osc v0.0.0-20200115085105-85fee7fed692
	gitlab.com/sacules/log v0.1.0
)
