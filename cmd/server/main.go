package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/hypebeast/go-osc/osc"
	"gitlab.com/sacules/log"
)

var (
	ipAddr        string
	portOsc       string
	portWebSocket string
	oscChan       chan osc.Packet
	record        bool
)

func init() {
	flag.StringVar(&ipAddr, "ip", "127.0.0.1", "ip de la cual escuchar los OSC")
	flag.StringVar(&portOsc, "port-osc", "", "port to intercept OSC packets from")
	flag.StringVar(&portWebSocket, "port-web", "", "port that will broadcast the packets (needs to be forwarded)")
	flag.BoolVar(&record, "record", false, "records and saved the received OSC packets to a file")
	flag.Parse()

	if portOsc == "" || portWebSocket == "" {
		fmt.Println("server: missing flags")
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	var packets []osc.Packet
	m := &sync.Mutex{}

	addr := ipAddr + ":" + portOsc

	oscChan = make(chan osc.Packet)

	server := &osc.Server{}
	conn, err := net.ListenPacket("udp", addr)
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}
	defer conn.Close()

	http.HandleFunc("/", handler)
	go http.ListenAndServe(":"+portWebSocket, nil)
	log.Info("ready to broadcast OSC on port", portWebSocket)

	go func() {
		if !record {
			return
		}

		f, err := os.OpenFile("record.txt", os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			log.Error(err)
			return
		}
		defer func() {
			err = f.Close()
			if err != nil {
				log.Error(err)
			}
		}()

		i := 0
		timer := time.NewTicker(5 * time.Second)
		for {
			select {
			case <-timer.C:
				m.Lock()
				if len(packets) == 0 {
					m.Unlock()
					continue
				}

				newpackets := packets[i:]
				i = len(packets) - 1
				m.Unlock()

				for _, p := range newpackets {
					msg := fmt.Sprintln(p)
					f.WriteString(msg)
				}
			}
		}
	}()

	for {
		packet, err := server.ReceivePacket(conn)
		if err != nil {
			log.Error("server:", err)
			os.Exit(1)
		}

		if packet == nil {
			continue
		}

		oscChan <- packet

		m.Lock()
		packets = append(packets, packet)
		m.Unlock()
	}
}

var up = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handler(w http.ResponseWriter, r *http.Request) {
	conn, err := up.Upgrade(w, r, nil)
	if err != nil {
		log.Error(err)
		return
	}
	defer conn.Close()

	log.Info("received connection from", r.RemoteAddr)

	for osc := range oscChan {
		bin, err := osc.MarshalBinary()
		if err != nil {
			log.Error(err)
			continue
		}

		conn.WriteMessage(websocket.BinaryMessage, bin)

		log.Info("sent:", osc)
	}

	log.Info("getting out!")
}
