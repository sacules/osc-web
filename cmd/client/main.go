package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"

	"github.com/gorilla/websocket"
	"github.com/hypebeast/go-osc/osc"
	"gitlab.com/sacules/log"
)

var (
	ipAddr        string
	portWebSocket string
	portOSC       int
)

func init() {
	flag.StringVar(&ipAddr, "ip", "", "adreed to connect to")
	flag.StringVar(&portWebSocket, "port-web", "", "port of the given ip adress")
	flag.IntVar(&portOSC, "port-osc", 0, "port on which the OSC will be exposed")
	flag.Parse()

	if ipAddr == "" || portWebSocket == "" || portOSC == 0 {
		fmt.Println("server: missing flags")
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	u := url.URL{Scheme: "ws", Host: ipAddr + ":" + portWebSocket, Path: "/"}
	log.Info("connecting to", u.String(), "...")

	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Error("dial:", err)
		return
	}
	defer conn.Close()
	log.Info("connection successful")

	client := osc.NewClient("localhost", portOSC)

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			log.Error(err)
			return
		}

		if t != websocket.BinaryMessage {
			log.Error("expected binary message, got a text:", msg)
			continue
		}

		packet, err := osc.ParsePacket(string(msg))
		if err != nil {
			log.Error("decoding OSC:", err)
			log.Error("got:", string(msg))
			continue
		}

		log.Info("received:", packet)
		client.Send(packet)
	}
}
